class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :title
      t.string :bytitle
      t.text :content
      t.decimal :price, precision: 8, scale: 2
      t.decimal :old_price, precision: 8, scale: 2
      t.string :status
      t.boolean :hit, default: false
      t.references :category, null: false, foreign_key: true
      t.references :brand, null: false, foreign_key: true

      t.timestamps
    end
  end
end
