class CreateNewsletters < ActiveRecord::Migration[6.0]
  def change
    create_table :newsletters do |t|
      t.text :email, null: false

      t.timestamps
    end
  end
end
