class CreateRelatedProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :related_products do |t|
      t.integer :related_id
      t.integer :product_id

      t.timestamps
    end
  end
end
