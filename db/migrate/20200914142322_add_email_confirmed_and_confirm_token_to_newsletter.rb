class AddEmailConfirmedAndConfirmTokenToNewsletter < ActiveRecord::Migration[6.0]
  def change
    add_column :newsletters, :email_confirmed, :boolean, default: false
    add_column :newsletters, :confirm_token, :string
  end
end
