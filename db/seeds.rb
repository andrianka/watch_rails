require 'faker'


[Product, Brand, Category].each do |model|
  model.destroy_all if model.first
end

Product.__elasticsearch__.create_index!(force: true)
# table brands
image_path = File.join(Rails.root, 'app', 'assets', 'images')

brand_title = ['Citizen', 'Casio', 'Royal London', 'Seiko', 'Diesel']
brand_images = ['abt-1.jpg', 'abt-2.jpg', 'abt-3.jpg', 'abt-3.jpg', 'abt-1.jpg']
5.times do |i|
  temp = Brand.create(
    title: brand_title[i],
    bytitle: brand_title[i].downcase,
    description: Faker::Lorem.sentence(word_count: 10)
  )
  temp.img.attach(
    io: File.open(File.join(image_path, brand_images[i])),
    filename: brand_images[i]
  )
end
# - - - - -

# table categories


women = Category.create(title: 'Women', bytitle: 'women', description: 'for women')
electronic2 = Category.create(title: 'Electronic', bytitle: 'electronic2',  description: 'electronic', parent: women)
mechanical2 = Category.create(title: 'Mechanical', bytitle: 'mechanical2', description: 'mechanical', parent: women)
epos = Category.create(title: 'Epos', bytitle: 'epos', description: 'epos',
        parent: mechanical2)
seiko = Category.create(title: 'Seiko', bytitle: 'seiko', description: 'seiko', parent: mechanical2)

men = Category.create(title: 'Men', bytitle: 'men', description: 'for men')
electronic1 = Category.create(title: 'Electronic', bytitle: 'electronic1', description: 'electronic', parent: men)
mechanical1 = Category.create(title: 'Mechanical', bytitle: 'mechanical1', description: 'mechanical', parent: men)
casio = Category.create(title: 'Casio', bytitle: 'casio', description: 'casio', parent: electronic1)
citizen = Category.create(title: 'Citizen', bytitle: 'citizen', description: 'citizen', parent: mechanical1)
royal = Category.create(title: 'Royal London', bytitle: 'royal-london', description: 'royal-london', parent: mechanical1)

kids = Category.create(title: 'Kids', bytitle: 'kids', description: 'for kids')
adriatica = Category.create(title: 'Adriatica', bytitle: 'adriatica', description: 'adriatica', parent: kids)
mechanical1 = Category.create(title: 'Anne Klein', bytitle: 'anne-klein',  description: 'anne-klein', parent: kids)

# - - - - - - - -


# table products

product_title = [
                'Casio MQ-24-7BUL', 'Casio GA-1000-1AER', 'Citizen JP1010-00E',
                'Citizen BJ2111-08E', 'Royal London 41040-01', 'Royal London 20034-02',
                'Royal London 41156-02', 'Royal London 6754-99', 'Royal London 12',
                # 'Royal London 6754-9922', 'Royal London 6754-939'
                ]
20.times do |i|
  product_img = "p-#{rand(1..8)}.png"
  title = product_title.sample
  temp = Product.create(
    category: Category.all.sample,
    brand: Brand.all.sample,
    title: title,
    bytitle: title.downcase,
    content: Faker::Lorem.sentence(word_count: 20),
    price: Faker::Commerce.price,
    old_price: Faker::Commerce.price,
    status: ['active', 'nonactive'].sample,
    description: Faker::Lorem.sentence(word_count: 10),
    hit: [true, false].sample
  )

  temp.imgs.attach(
    io: File.open(File.join(image_path, product_img)),
    filename: product_img
  )
end
# - - - - -
#Elasticsearch import data

Product.import

# Related products

20.times do
  RelatedProduct.create(product: Product.all.sample, related: Product.all.sample)
end
# -------------------
