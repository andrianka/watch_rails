FactoryBot.define do
  factory :category do
    title { Faker::Lorem.sentence(word_count: 10) }
    bytitle { title.downcase }
    description { Faker::Lorem.sentence(word_count: 10) }
  end
end
