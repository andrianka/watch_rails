FactoryBot.use_parent_strategy = false
FactoryBot.define do
  factory :product do
    title       { Faker::Lorem.sentence(word_count: 10) }
    bytitle     { title.downcase }
    content     { Faker::Lorem.sentence(word_count: 10) }
    description { content }
    price       { rand(10..500) }
    old_price   { rand(10..500) }
    status      { 'active' }
    hit         { true }
    brand
    category
  end
end
