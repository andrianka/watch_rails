FactoryBot.define do
  factory :newsletter do
    email { 'test@example.com' }
  end
end
