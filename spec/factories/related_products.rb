FactoryBot.define do
  factory :related_product do
    related_id { 1 }
    product_id { 2 }
  end
end
