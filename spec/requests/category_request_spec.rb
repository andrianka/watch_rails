require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe 'GET#show' do
    let(:category) { create(:category) }

    it 'return status ok' do
      # it { should have_http_status(200) }
      get category_path(category.id)
      expect(response).to have_http_status(:ok)
    end
  end
end
