require 'rails_helper'

RSpec.describe "Mains", type: :request do

  describe 'GET #index' do
    
    before { get root_path }

    context 'required information' do
      it 'return status ok' do
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
