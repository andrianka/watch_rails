require 'rails_helper'


RSpec.describe "Carts", type: :request do
  let(:user) { create(:user) }
  let(:product) { create(:product) }
  let(:cart) { create(:cart, user: user)}
  let(:item) { create(:cart_item, cart: cart, product: product) }

  describe 'GET #show' do
    before { get cart_path(cart.id) }

    it { should have_http_status(200) }
    it 'contain an item' do
      expect(response.body).to include(product.title)
    end
  end
end
