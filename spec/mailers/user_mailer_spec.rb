require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe "newsletter_confirmation" do
    let(:newsletter) {  create(:newsletter) }

    before do
      newsletter.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
    let(:mail) { UserMailer.newsletter_confirmation(newsletter) }

    it "renders the headers" do
      expect(mail.subject).to eq("Confirm your Subscribe")
      expect(mail.to).to eq(["test@example.com"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
