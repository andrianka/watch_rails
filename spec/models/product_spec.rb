require 'rails_helper'

RSpec.describe Product, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
  end
  describe 'Associations' do
    it { should have_many(:related) }
    it { should have_many(:related_products) }
    it { should belong_to(:category) }
    it { should belong_to(:brand) }
  end
  describe 'DB Table' do
    it { is_expected.to have_db_column(:title).of_type(:string) }
    it { is_expected.to have_db_column(:description).of_type(:text) }
  end

  describe 'Attachments' do
    it 'is valid' do
      subject.imgs.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'p-1.png')), filename: 'attachment.jpg', content_type: 'image/png')
      expect(subject.imgs).to be_attached
    end
  end
end
