require 'rails_helper'

RSpec.describe Brand, type: :model do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }

  describe 'Associations' do
    it { should have_many(:products).dependent(:destroy) }
  end
end
