require 'rails_helper'

RSpec.describe Newsletter, type: :model do
  subject { create(:newsletter) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email) }
end
