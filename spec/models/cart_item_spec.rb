require 'rails_helper'

RSpec.describe CartItem, type: :model do
  subject { build(:cart_item) }

  describe 'Validations' do
    it { should validate_presence_of(:quantity) }
    it { should validate_numericality_of(:quantity).only_integer }
  end

  describe 'Associations' do
    it { should belong_to(:product) }
    it { should belong_to(:cart) }
  end

end
