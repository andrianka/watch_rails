require 'rails_helper'

RSpec.describe Cart, type: :model do
  describe 'Associations' do
    it { should have_many(:cart_items) }
    it { should have_many(:products).through(:cart_items) }
    it { should belong_to(:user) }
  end
end
