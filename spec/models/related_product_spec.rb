require 'rails_helper'

RSpec.describe RelatedProduct, type: :model do
  describe 'Associations' do
    it { should belong_to(:related).class_name('Product') }
    it { should belong_to(:product) }
  end
end
