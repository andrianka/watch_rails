require 'rails_helper'

RSpec.describe User, type: :model do
  subject { create(:user) }

  describe 'Validations' do
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end

  describe 'Associations' do
    it { should have_many(:carts) }
    it { should have_many(:products).through(:carts) }
  end
end
