require 'rails_helper'

RSpec.describe Category, type: :model do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:description) }

  describe 'Associations' do
    it { should have_many(:products) }
  end
end
