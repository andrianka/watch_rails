/*Search*/
$(document).on('turbolinks:load', function(){
  var products = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      wildcard: '%QUERY',
      url: '/search?query=%QUERY'
    }
  });

  products.initialize();

  $('#typeahead').typeahead({
    highlight: true,
    minLength: 3
  }, {
    name: 'products',
    display: 'title',
    limit: Infinity,
    source: products
  });

  $('#typeahead').on('typeahead:select', function(ev, suggestion){
    window.location = 'brand/' + suggestion.brand_id + '/product/' + suggestion.id
  });
});
