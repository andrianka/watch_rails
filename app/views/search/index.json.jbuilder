json.array! @products do |product|
  json.extract! product, :id, :title, :brand_id
end
