class CartItemsController < ApplicationController
  layout false

  def create
    current_cart.cart_items.create(cart_items_params)
  end

  def destroy
    current_cart.cart_items.find_by(cart_items_params).destroy
    render :create
  end

  private

  def cart_items_params
    params.require(:cart_item).permit(:quantity, :product_id)
  end
end
