class ProductController < ApplicationController
  before_action :set_product, only: [:show]
  after_action :register_visit_to_session, only: [:show]

  def show
    set_page_options
  end

  private

  def set_page_options
    add_breadcrumb 'Home', root_path, title: 'Home'
  end

  def register_visit_to_session
    session[:viewed_products] ||= []
    session[:viewed_products] = (session[:viewed_products] + [@product.id])
                                .uniq
                                .take(3)
  end

  def set_product
    brand = Brand.find(params[:brand_id])
    @product = brand.products.find(params[:id])
    @recent_products = Product.recent(session[:viewed_products]).with_attached_imgs
  end

  def product_params
    params.require(:product).permit(:brand_id)
  end
end
