class NewslettersController < ApplicationController

  def create_subscribe
    result = NewsletterService::Subscribe.call(Newsletter.new(newsletter_params))
    if result.success?
      flash[:success] = 'Thanks, for subscribe! Please confirm your email address'
    else
      flash[:error] = result.errors
    end
    redirect_to root_path
  end

  def cancel_subscribe
  end

  def confirm_email
    result = NewsletterService::ConfirmEmail.call(params[:token])
    if result.success?
      flash[:success] = "You successfuly confirmed email!"
    else
      flash[:error] = result.errors
    end
    redirect_to root_path
  end
  private

  def newsletter_params
    params.fetch(:newsletter, {}).permit(:email)
  end

end
