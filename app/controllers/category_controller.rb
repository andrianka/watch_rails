class CategoryController < ApplicationController
  before_action :set_category, only: [:show]

  def show
    set_page_options
  end

  private

  def set_category
    @category = Category.includes(:products).find(params[:id])
    @category_products = @category.products
  end

  def set_page_options
    add_breadcrumb 'Home', root_path, title: 'Home'
  end

  def category_params
    params.require(:category).permit()
  end
end
