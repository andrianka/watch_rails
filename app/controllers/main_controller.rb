class MainController < ApplicationController

  def index
    @brands = Brand.limit(3)
    @hits = Product.hits.limit(8).with_attached_imgs
  end

end
