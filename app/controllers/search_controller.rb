class SearchController < ApplicationController
  before_action :set_page_options

  def index
    response = Product.__elasticsearch__.search(
      query: {
        multi_match: {
          query: params[:query],
          fields: ['title', 'description', 'brand.title']
        }
      }
    )
    @products = response.records.to_a
    respond_to do |format|
      format.html
      format.json
    end
  end

  private

  def set_page_options
    add_breadcrumb 'Home', root_path, title: 'Home'
  end
end
