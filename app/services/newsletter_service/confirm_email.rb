require 'pry'
class NewsletterService::ConfirmEmail

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(token)
    @newsletter = Newsletter.find_by_confirm_token(token)
  end

  def execute
    validate_email
    return OpenStruct.new(success?: false, newsletter: nil, errors: "Sorry. Subscriber or token does not exist") unless @newsletter && @newsletter.save
    OpenStruct.new(success?: true, newsletter: @newsletter, errors: nil)
  end

  private

  def validate_email
    if @newsletter
      @newsletter.email_confirmed = true
      @newsletter.confirm_token = nil
    end
  end
end
