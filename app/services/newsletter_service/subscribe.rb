class NewsletterService::Subscribe

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(newsletter)
    @newsletter = newsletter
    @newsletter.confirm_token = set_confirmation_token
  end

  def execute
    return OpenStruct.new(success?: false, newsletter: nil, errors: @newsletter.errors) unless @newsletter.save
    send_newsletter_confirm
    OpenStruct.new(success?: true, newsletter: @newsletter, errors: nil)
  end

  private
  def set_confirmation_token
    SecureRandom.urlsafe_base64.to_s if @newsletter.confirm_token.blank?
  end

  def send_newsletter_confirm
    UserMailer.newsletter_confirmation(@newsletter).deliver_now 
  end
end
