class UserMailer < ApplicationMailer

  def newsletter_confirmation(newsletter)
    @subscriber = newsletter
    mail(to: @subscriber.email, subject: 'Confirm your Subscribe')
  end

end
