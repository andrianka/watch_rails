module CartsHelper

  def products
    @products ||= items.joins(:product).all
  end

  def get_cart_quantity_sum(products)
    products.inject(0){ |sum, p| sum + p.quantity }
  end

  def get_cart_summary(products)
    products.inject(0){ |sum, p| sum + p.price * p.quantity }
  end
end
