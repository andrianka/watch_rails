module ProductsHelper

  def get_img_or_title_link(cart_item)
    if cart_item.imgs.nil?
      link_to cart_item.title, brand_product_path(cart_item.brand_id, cart_item.product_id)
    else
      link_to(image_tag(cart_item.imgs.first), brand_product_path(cart_item.brand_id, cart_item.product_id))
    end
  end

end
