module UsersHelper

  def get_button_title(method)
    if method == :post
      'Sign Up!'
    elsif method == :put
      'Update'
    end
  end

  def method_put?(method)
    method == :put
  end
end
