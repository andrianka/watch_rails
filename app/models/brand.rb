class Brand < ApplicationRecord
  has_many :products, dependent: :destroy
  has_one_attached :img

  validates :title, presence: true
  validates :description, presence: true
end
