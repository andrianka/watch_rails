class Category < ApplicationRecord
  has_ancestry
  has_many :products, dependent: :destroy

  validates :title, presence: true
  validates :description, presence: true

  scope :parent_root, -> { where(ancestry: nil) }
end
