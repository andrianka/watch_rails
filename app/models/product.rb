require 'elasticsearch/model'
class Product < ApplicationRecord
  include ActiveModel::Validations

  has_many :related_products, dependent: :destroy
  has_many :related, through: :related_products
  belongs_to :category
  belongs_to :brand

  has_many_attached :imgs
  enum status: { active: 'active', nonactive: 'nonactive'}, _suffix: true

  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  validates :title, presence: true
  validates :description, presence: true
  validates :status, inclusion: { in: Product.statuses }

  scope :hits, -> { where(hit: true) }
  scope :recent, -> (viewed_products) { where(id: viewed_products) }

  def as_indexed_json(options = {})
    self.as_json(
      only: [:id, :title, :description],
      include: {
        brand: {
          only: [:title]
        }
      }
    )
  end
end
