class CartItem < ApplicationRecord
  belongs_to :product
  belongs_to :cart

  validates :product,  presence: true
  validates :cart,     presence: true
  validates :quantity, presence: true, numericality: { only_integer: true  }

  delegate :title, :imgs, :price, :brand_id, to: :product

end
