class RelatedProduct < ApplicationRecord
  belongs_to :related, class_name: 'Product'
  belongs_to :product
end
