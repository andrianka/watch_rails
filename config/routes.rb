Rails.application.routes.draw do
  devise_for :users
  namespace :admin do
    resources :related_products
    resources :categories
    resources :products
    resources :newsletters
    resources :brands
    root to: "products#index"
  end
  root to: 'main#index'
  resources :brand, only: [:index, :show] do
    resources :product, only: [:show]
  end
  resources :category, only: [:show]
  resources :search, only: [:index]
  resource :cart, only: [:show, :destroy] do
    resources :cart_items, only: [:create, :destroy], path: 'items', as: 'items'
  end
  post 'subscribe', to: 'newsletters#create_subscribe'
  delete 'cancel_subscribe', to: 'newsletters#cancel_subscribe'
  get '/:token/confirm_email/', to: 'newsletters#confirm_email', as: 'confirm_email'
end
